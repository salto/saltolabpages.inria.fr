<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>The Salto project</title>
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
    <script src="include_html.js"></script>
    <div w3-include-html="navbar.html"></div>

    <h1><em>Salto</em>: Static Analyses for Trustworthy OCaml</h1>

    <figure>
      <img alt="camel robot" src="img/salto_camel_2024.webp" height="300">
      <figcaption>A painting-style depiction of an armored camel
      standing in a desert.<br>(image produced
      by <a href="https://openai.com/dall-e">DALL·E</a>)</figcaption>
    </figure>

    <p>
      The expressive type system
      of <a href="https://www.ocaml.org/">OCaml</a> brings strong
      static safety guarantees by ensuring data is used in a
      consistent way by a program. The <em>Salto</em> project aims at
      providing OCaml programs with additional guarantees, by
      answering questions such as:
    </p>
    <ul>
      <li>
        <em>Can a program raise an exception?</em>
      </li>
      <li>
        <em>Can arithmetic operations overflow?</em>
      </li>
      <li>
        <em>Are all the array accesses performed within the array bounds?</em>
      </li>
      <li>
        <em>Can a program break some user-defined invariant or assertion?</em>
      </li>
      <li>
        <em>Which data might be modified by a program?</em>
      </li>
    </ul>
    <p>
      Those questions are beyond the scope of the OCaml type system,
      but are within reach of abstract interpretation-based static
      analyses.
    </p>
    <p>
      The goal of the <em>Salto</em> project is to define sound static
      analyses that help answer the above questions as automatically
      as possible. The analysis will help a programmer have better
      confidence in her programs, by bringing increased safety and
      security guarantees.
    </p>

    <h2>News</h2>
    <ul>
      <li><em>March 2025:</em> After 3 years of intensive efforts, we
        are finally releasing a public version of the Salto analyzer!
        It has already good (but still partial) support for OCaml
        4.14. <em>Disclaimer: The analyzer should still be considered
        experimental.</em>
        <br>
        Check out
        the <a href="https://gitlab.inria.fr/salto/salto-analyzer">source
        repository</a>, or wait for the OPAM packages to be available
        (soon, hopefully!).
      </li>
      <li><em>November 2024:</em> The funding from Inria/Nomadic Labs
        has ended. These two years have been fruitful, and the funding
        helped the project make great progress. Great thanks!
        <br>
        The <em>Salto</em> project has received a 1 year funding from
        the <a href="https://ocaml-sf.org/">OCaml Software
        Foundation</a>. Many thanks to the foundation!
      </li>
      <li><em>October 2024:</em> The
        article <a href="https://ercim-news.ercim.eu/en139/special/the-salto-project-static-analysis-of-ocaml-programs-by-abstract-interpretation"><strong>The
        Salto Project: Static Analysis of OCaml Programs By Abstract
        Interpretation</strong></a> was accepted for publication
        in <a href="https://ercim-news.ercim.eu/images/stories/EN139/EN139-web.pdf">ERCIM
        News</a>! Special issue on Software Security, number 139.
      </li>
      <li><em>December 2023:</em> The
          paper <a href="https://www.doi.org/10.1007/978-3-031-57267-8_15"><strong>&quot;Detection
          of Uncaught Exceptions in Functional Programs by Abstract
          Interpretation&quot;</strong></a>
          (by <a href="https://plermusi.github.io/">Pierre
          Lermusiaux</a>
          and <a href="https://people.irisa.fr/Benoit.Montagu/">Benoît
          Montagu</a>) has been accepted
          at <a href="https://etaps.org/2024/conferences/esop/">ESOP2024</a>!
        <br>
        [ <a href="https://www.doi.org/10.1007/978-3-031-57267-8_15">DOI</a>
        | <a href="https://hal.science/hal-04410771">Extended
        version</a>
        | <a href="https://zenodo.org/doi/10.5281/zenodo.10457925">Artifact</a>
        ]
      </li>
      <li><em>December 2023:</em> release of <code>salto-IL</code>,
      the intermediate language that is used by the Salto analyser,
      and the transformation from the OCaml Typedtree to this
      language. The intermediate language is still subject to changes,
      so this should be considered as experimental. Any feedback is
      welcome!
        <br>
        [ <a href="https://gitlab.inria.fr/salto/salto-il">Repository</a> ]
      </li>
      <li>
        <em>September 2023:</em> research talk on Salto at
        the <a href="https://icfp23.sigplan.org/home/mlworkshop-2023#program">ML
        workshop</a> (co-located
        with <a href="https://icfp23.sigplan.org/">ICFP'23</a>,
        Seattle)
        <br>
        <strong>The Design and Implementation of an Abstract
        Interpreter for OCaml Programs: A Preliminary Report on the
        Salto Analyser</strong>
        <br>
        [ <a href="publications/ML2023_Salto_preliminary_report.pdf">Extended
        abstract</a>
        | <a href="talks/2023-09-07_ML_workshop_Seattle.pdf">Slides</a>
        | <a href="https://youtu.be/M5M3f31pxns?list=PLyrlk8Xaylp7Ea-Zu98G_VuxGtUnK_jB8&t=30018">Video</a>
        ]
      </li>
      <li><em>November 2022:</em> official start of the project</li>
    </ul>

    <h2>Contributors</h2>
    <ul>
      <li><a href="https://people.rennes.inria.fr/Thomas.Jensen/">
          Thomas Jensen</a>, senior researcher at Inria,
        team <a href="https://team.inria.fr/epicure/">Épicure</a>
      </li>
      <li><a href="https://people.irisa.fr/Thomas.Genet/">Thomas
          Genet</a>, full professor at Université Rennes,
        team <a href="https://team.inria.fr/epicure/">Épicure</a>
      </li>
      <li><a href="https://plermusi.github.io/">Pierre
        Lermusiaux</a>, research engineer at Inria,
        team <a href="https://team.inria.fr/epicure/">Épicure</a>
      </li>
      <li><a href="https://people.irisa.fr/Benoit.Montagu/">Benoît
          Montagu</a> (lead), researcher at Inria,
        team <a href="https://team.inria.fr/epicure/">Épicure</a>
      </li>
    </ul>

    <h2>Former contributors</h2>
    <ul>
      <li>
        <a href="https://perso.eleves.ens-rennes.fr/people/tom.goalard/">Tom
          Goalard</a>, L3 student,
        <a href="https://www.ens-rennes.fr/">ENS Rennes</a>
      </li>
    </ul>

    <h2>Funding</h2>
    <p>
      The <em>Salto</em> project initially started as a partnership
      between
      <a href="https://www.nomadic-labs.com/">Nomadic Labs</a>
      and <a href="https://www.inria.fr/">Inria</a>, and was funded
      for 2 years by the
      <a href="https://tezos.foundation/">Tezos Foundation</a>.
      <br>
      Since November 2024, the <em>Salto</em> project was granted a 1
      year funding from the <a href="https://ocaml-sf.org/">OCaml
      Software Foundation</a>.
    </p>

    <div w3-include-html="footer.html"></div>

<script>
includeHTML();
</script>

  </body>
</html>

<!--  LocalWords:  Salto OCaml Benoît Montagu Inria Épicure Rennes
 -->
<!--  LocalWords:  Université Tezos ICFP Lermusiaux
 -->
