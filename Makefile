BIBTEX=bibtex2html
BIBTEXOPTS=-d -r -nobiblinks -nokeys -use-table -noabstract -nokeywords -nobibsource -nodoc -noheader -nofooter --named-field youtube "Video (Youtube)" --named-field video "Video (local)" --named-field slides "Slides" --named-field artifact "Artifact" --named-field url "At publisher's" --named-field pdf "Local PDF" --named-field hal "On HAL" --named-field extended "Extended version"

all: public/publications/publis.html

public/publications/publis.html: publis.bib Makefile owens_web.bst
	$(BIBTEX) $(BIBTEXOPTS) -s owens_web -o public/publications/publis $<

.PHONY: all
